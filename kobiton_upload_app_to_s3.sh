#!/bin/bash
# Install ack
#curl https://beyondgrep.com/ack-2.22-single-file > /usr/local/bin/ack && chmod 0755 /usr/local/bin/ack
KUSERNAME=$KUSERNAME
KAPIKEY=$KAPIKEY
APPNAME=$APPNAME
APPPATH=$APPPATH
APPID=""
#test enviroment variables
echo "===================Start of test enviroment variables======================"
echo $KUSERNAME
echo $KAPIKEY
echo $APPNAME
echo $APPPATH
echo "===================End of test enviroment variables==========================="

echo '=============================Step 1: Generate Basic Authorization========================================'

BASICAUTH="$(echo -n $KUSERNAME:$KAPIKEY | base64)"
header="Authorization: Basic $BASICAUTH"
echo $BASICAUTH

echo '=========================Step 2: Generate Upload URL==================================='
if [ -z "$APPID" ]; then
  JSON="{\"filename\":\"${APPPATH}\"}"
else
  JSON="{\"filename\":\"${APPPATH}\",\"appId\":$APPID}"
  #testing here
  echo  $JSON
fi

curl -X POST https://api.kobiton.com/v1/apps/uploadUrl \
  -H "$header" \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d "$JSON" \
  -o ".tmp.response.json"

 #testing here
  echo $curl -X POST https://api.kobiton.com/v1/apps/uploadUrl \
  -H "$header" \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d "$JSON" \
  -o ".tmp.response.json"

UPLOADURL=$(cat ".tmp.response.json" | ack -o --match '(?<=url\":")([_\%\&=\?\.aA-zZ0-9:/-]*)')
KAPPPATH=$(cat ".tmp.response.json" | ack -o --match '(?<=appPath\":")([_\%\&=\?\.aA-zZ0-9:/-]*)')

echo $curl -X POST https://api.kobiton.com/v1/apps/uploadUrl \
  -H "$header" \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d "$JSON" \
  -o ".tmp.response.json"
echo '=======================================Step 3: Upload File To S3==========================================='
echo
echo "uploading......"
curl -T "${APPPATH}" \
  -H 'content-type: application/octet-stream' \
  -H 'x-amz-tagging: unsaved=false' \
  -X PUT "${UPLOADURL}"

  #testing here
echo $curl -T "${APPPATH}" \
  -H 'content-type: application/octet-stream' \
  -H 'x-amz-tagging: unsaved=true' \
  -X PUT "${UPLOADURL}"


echo '==============================================Step 4: Create Application Or Version============================='

JSON="{\"filename\":\"${APPNAME}\",\"appPath\":\"${KAPPPATH}\"}"

#testing here
echo $JSON

echo '====================================================Step 5 : Ask Kobiton to link the upload file to App Repository==========================='
curl -X POST https://api.kobiton.com/v1/apps \
  -H "$header" \
  -H 'content-type: application/json' \
  -d "$JSON"
echo
echo 'The file has been uploaded successfully to Kobiton....Done'

echo
#curl -X GET https://api.kobiton.com/v1/apps \
#  -H "$header" \
#  -H 'Accept: application/json'

echo
#Get information about an app.
#curl -X GET https://api.kobiton.com/v1/apps/{APPID} \
#  -H "$header" \
#  -H 'Accept: application/json'

#Get All Devices.Retrieve devices in 3 groups: private/org, favorite and cloud devices.
#curl -X GET https://api.kobiton.com/v1/devices \
#  -H "$header" \
#  -H 'Accept: application/json'
